import createTheme, { Theme, ThemeOptions } from '@material-ui/core/styles/createTheme';

export const customTheme: ThemeOptions = createTheme({
  palette: {
    primary: {
      main: '#38798e',
      contrastText: '#fff'
    },
    secondary: {
      main: '#F5B59F'
    },
    background: {
      default: '#275463'
    }
  },
  typography: {
    fontFamily: '"Raleway", "Open Sans", "Calibri"',
    fontWeightMedium: 500,
    fontWeightBold: 600,
    fontWeightRegular: 400
  }
});

export type CustomThemeType = Theme;

//Visit to find effect of different variables on theme:  https://bareynol.github.io/mui-theme-creator/
