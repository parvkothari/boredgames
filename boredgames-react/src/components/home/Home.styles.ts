//import { ExtTheme } from '@wideorbit/wo-react-shared';
import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => {
  return {
    homeRoot: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      height: '100vh',
      '& > *' : {
        margin: '10px 0'
      }
      
    },
    scrabbleImage: {
      width: 308,
      height: 283,
      cursor: 'pointer'
    }
  }
});
