import React from 'react';
import { Button, Container, Typography } from '@material-ui/core';
import { useStyles } from './Home.styles';
import { scrabbleBoard } from '../../assets';

export const Home = () => {
  const classes = useStyles();

  return (
    <Container maxWidth="sm" className={classes.homeRoot}>
      <Typography variant="h4">Welcome to Bored Games</Typography>
      <img src={scrabbleBoard} alt="Scrabble Board" className={classes.scrabbleImage} />
      <Button color="primary" variant="contained">
        Play Scrabble
      </Button>
    </Container>
  );
};
