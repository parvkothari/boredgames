import { Home } from '../home';
import { ThemeProvider } from '@material-ui/core/styles';
import { customTheme } from '../../theme';

export const App = () => {
  return (
    <ThemeProvider theme={customTheme}>
      <Home />
    </ThemeProvider>
  );
};
